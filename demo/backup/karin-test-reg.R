library(magrittr)
karin = read.csv("demo/backup/karin/1358.csv", stringsAsFactors = F, check.names = T)

runIt2 <- function() {

    accum = ena.accumulate.data.file(
      file = karin,
      units.by = c("Condition","sex", "speaker"),
      conversations.by = c("Participant","Condition"),
      codes = c("sad","cold.hearted","excited", "angry", "relieved"),
      window = "Conversation"
    )

}

profvis::profvis({
  runIt2()
})
plot.set(set, scale.to = "points") %>%
  with.groups() %>%
  with.units()

