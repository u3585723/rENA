from asyncio.windows_events import NULL
import rpy2.robjects as robjects
from rpy2.robjects.packages import importr
import plotly.express as px

fig = px.bar(x=["a", "b", "c"], y=[1, 3, 2])
fig.write_html('first_figure.html', auto_open=True)

base = importr('base')
# utils = importr('utils')

rena = importr('rENA')

rsdata = robjects.DataFrame(robjects.r['RS.data'])

units = robjects.StrVector(['Condition', 'UserName'])
convs = robjects.StrVector(['Condition', 'GroupName'])
codes = robjects.StrVector(['Data', 'Performance.Parameters', 'Technical.Constraints'])

unit_cols_i = robjects.IntVector((2,1))
units_sub_df = rsdata.rx(True, unit_cols_i)
convs_cols_i = robjects.IntVector((2,11))
convs_sub_df = rsdata.rx(True, convs_cols_i)
codes_cols_i = robjects.IntVector((15,16,17))
codes_sub_df = rsdata.rx(True, codes_cols_i)

ena_accum = robjects.r('ena.accumulate.data')
accum = ena_accum(units_sub_df, convs_sub_df, codes_sub_df)

ena_set = robjects.r('ena.make.set')
model = ena_set(accum)

ena_plot = robjects.r('ena.plot')
ena_plot_points = robjects.r('ena.plot.points')

model_points = model.rx('points')[0]
model_points_mtx = robjects.r('as.matrix')(model_points)
model_plot = ena_plot(model)
model_plot = ena_plot_points(model_plot, model_points_mtx)

# model_html = htmltools::html_print(htmltools::as.tags(plot.rotated$plot, standalone = TRUE), viewer = NULL)
html_print = robjects.r('htmltools::html_print')
as_tags = robjects.r('htmltools::as.tags')
model_html = html_print(as_tags(model_plot['plot'], standalone = True), viewer = robjects.r("NULL"))[0]

import webbrowser
webbrowser.open_new_tab(model_html)
