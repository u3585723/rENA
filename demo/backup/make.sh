#!/bin/bash

#ls -al ~/
rm -f ./R/RcppExports.R
#R --version
#Rscript -e 'Sys.info()'

mkdir -p ~/.R/
rm -f ~/.R/Makevars
cat ~/.R/Makevars
cp demo/backup/Makevars ~/.R/
cat ~/.R/Makevars

Rscript -e 'Rcpp::compileAttributes()'
Rscript -e 'update(devtools::dev_package_deps("."))'
#ls -al ./man/
#ls -al ../
cd ..; R CMD build --resave-data --no-manual --log rENA || true
R --no-site-file --no-environ --no-save --no-restore --quiet CMD check rENA_0.1.6.tar.gz --as-cran --no-manual |& tee build.txt
#ls -al ./
#cat ./rENA.Rcheck/rENA-00build.log || true
#cat ./rENA.Rcheck/00check.log || true
#cat ./rENA.Rcheck/00install.out || true
#cat ./build.txt
