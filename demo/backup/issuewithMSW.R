library(rENA)
nadav = read.csv("inst/extdata/sample-data/nadavcomb.csv")
# n_codes = c("t.assignment.directions", "s.explains.thinking", "norms", "t.asks.a.question", "s.asks.a.question"
              # , "t.listens.or.clarifies", "t.answers.a.question", "s.answers.a.question", "asks.for.progress")
n_codes = c('t.asks.for.explanation','t.listens.or.clarifies','t.assignment.directions')
n_accum = ena.accumulate.data(units = data.frame(nadav[,c("TeacherListenorInteracts", "Speaker", "group", "timeSegment")]),
                              conversation = data.frame(nadav[,c('timeSegment')]),
                              codes = data.frame(nadav[,n_codes]), window.size.back = 4)
n_set = ena.make.set(enadata = n_accum)
plot = ena.plot(n_set)
n_points = n_set$points.rotated
plot = ena.plot.points(plot, points = n_points)
print(plot)

joyner = read.csv("inst/extdata/sample-data/Coded-Slice3.csv")
j_codes = c("Con.plus", "Con.minus", "Gen.plus", "InE.plus", "TAE.plus", "Adm.minus", "Ass.plus", "Pro.plus"
            , "Rig.minus", "Lec.minus", "Lec.plus", "Rig.plus")
j_accum = ena.accumulate.data(units = data.frame(joyner[,c("difficulty", "course", "semester")]),
                              conversation = data.frame(joyner[,c("key")]), codes = data.frame(joyner[,j_codes])
                              , window.size.back = 4)
j_set = ena.make.set(enadata = j_accum)
plot = ena.plot(j_set)
j_points = j_set$points.rotated
plot = ena.plot.points(plot, points = j_points)
print(plot)
