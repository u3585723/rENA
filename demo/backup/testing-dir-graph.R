library(data.table)

adj <- matrix(c(1, 2, 0, 1, 2, 3, 0, 1, 2), ncol = 3, dimnames = list(LETTERS[1:3], LETTERS[1:3]))
adj

connections <- expand.grid(1:3, 1:3)
colnames(connections) <- c("Send", "Recv")

weights <- matrix(apply(connections, 1, function(r) adj[r[1], r[2]]), ncol = 1)

connections_weights <- cbind(connections, weights)
connections_weights$Send <- LETTERS[connections_weights$Send]
connections_weights$Recv <- LETTERS[connections_weights$Recv]
connections_weights_dt <- data.table(connections_weights)

nodes_send_wt <- connections_weights_dt[, lapply(.SD, sum) , by = Send, .SDcols = c("weights")]
setkey(nodes_send_wt, "Send")
nodes_recv_wt <- connections_weights_dt[, lapply(.SD, sum) , by = Recv, .SDcols = c("weights")]
setkey(nodes_recv_wt, "Recv")

nodes_wts <- nodes_send_wt[nodes_recv_wt, ]
colnames(nodes_wts) <- c("node", "wt_send", "wt_recv")
nodes_wts[, wt := sum(.SD), by = "node"]
nodes_xy <- matrix(sample(1:10, nrow(nodes_wts) * 2, replace = TRUE), ncol = 2)
nodes_wts[, x := nodes_xy[, 1]]
nodes_wts[, y := nodes_xy[, 2]]

node_edges$x0 = apply(node_edges, 1, function(r) nodes_wts[node == r[1]]$x)
node_edges$x1 = apply(node_edges, 1, function(r) nodes_wts[node == r[2]]$x)
node_edges$y0 = apply(node_edges, 1, function(r) nodes_wts[node == r[1]]$y)
node_edges$y1 = apply(node_edges, 1, function(r) nodes_wts[node == r[2]]$y)

library(plotly)
axes <- list(
  type = "linear",
  autorange = FALSE,
  range = c(0,10)
);
p <- plot_ly() %>%
  add_markers(x = ~x, y = ~y, size = ~wt, marker = list(sizemode = "diameter"), data = nodes_wts) %>%
  add_text(x = ~x, y = ~y, text = ~node, textposition = "top right", data = nodes_wts) %>%
  layout(xaxis = axes, yaxis = axes)

for (n in 1:nrow(node_edges)) {
  e = node_edges[n,];
  p <- plotly::add_trace(
    p, type = "scatter", mode = "lines",
    data = data.frame(X1=c(e$x0,e$x1), X2=c(e$y0,e$y1)),
    line = list(width = nodes_wts[node == e$V1,][["wt"]]),
    x = ~X1, y = ~X2
  )
}

p
